#include <iostream>
using namespace std;

bool isNotFound;
class Node
{
    public:
        int value;
        Node * left;
        Node * right;
};

/*
    A function to create a new node with given value
*/
Node * createNode(int value)
{
    Node * node = new Node;
    node -> value = value;
    node -> left = node -> right = NULL;
    return node;
}

/*
    A function to traverse the tree in inorder fashion
*/
void traverse(Node *root)
{
    if (root != NULL)
    {
        traverse(root->left);
        cout << root -> value << ", ";
        traverse(root->right);
    }
}

/*
    A function to insert a node in BST
*/
Node * insertNode(Node* node, int value)
{
    if (node == NULL)
        return createNode(value);
    if (value < node->value)
        node->left  = insertNode(node->left, value);
    else
        node->right = insertNode(node->right, value);
    return node;
}

/*
    A function to get the smallest node from the right sub-tree of 'root' node
*/
Node * getSmallestLeaf(Node* root)
{
    Node* current = root -> right;
    while (current->left != NULL)
        current = current->left;
    return current;
}

/*
    A function to remove a node from the tree with root node as 'root'
*/
Node * removeNode(Node * root, int value, Node * parent)
{
    if (root == NULL)
    {
        isNotFound = true;
        return root;
    }

    if (value < root -> value)
        root -> left = removeNode(root -> left, value, root);
    else if (value > root -> value)
        root -> right = removeNode(root -> right, value, root);
    else
    {
	if (root -> left != NULL && root->right != NULL)
	{
            Node * inorderSuccessor = getSmallestLeaf(root);
            root -> value = inorderSuccessor -> value;
            root -> right = removeNode(root -> right, inorderSuccessor -> value, root);
	}
	else if (root -> left == NULL)
	{
            Node * temp = root;
            root = root -> right;

            if (parent != NULL)
            {
                if (parent -> value > temp -> value)
                    parent -> left = root;
                else
                    parent -> right = root;
            }
            delete temp;
            return root;
	}
	else if (root -> right == NULL)
	{
            Node * temp = root;
            root = root -> left;
            if (parent != NULL)
            {
                if (parent -> value > temp -> value)
                    parent -> left = root;
                else
                    parent -> right = root;
            }
            delete temp;
            return root;
	}
    }
    return root;
}

/*
    A function to process the output of removeNode function
*/
void analyseOutput(Node *& root, int value)
{
    isNotFound = false;

    cout<<endl<<"Delete " << value << ": ";
    if (root == NULL)
    {
        cout << "Tree is empty!!";
    }
    else
    {
        root = removeNode(root, value, NULL);
        if (root == NULL)
            cout << "Tree is empty!!";
        else if (isNotFound)
            cout << "Match not found!!";
        else
            traverse (root);
    }
}

/*
    A function to create a BST structured:

                        5
                2             6
            1       4               15
                                9
                            7       12
*/
Node * createBST()
{
    Node * root = NULL;
    root = insertNode(root, 5);
    root = insertNode(root, 2);
    root = insertNode(root, 6);
    root = insertNode(root, 1);
    root = insertNode(root, 4);
    root = insertNode(root, 15);
    root = insertNode(root, 9);
    root = insertNode(root, 7);
    root = insertNode(root, 12);
    cout<<"Original tree: ";
    traverse (root);
    return root;
}

int main()
{
    cout << "All the representations below are in inorder fashion.\n\n";
    Node * root = createBST();

    analyseOutput(root, 6);//delete non-root node with no left child in right sub-tree
    analyseOutput(root, 15);//delete non-root node with no right child in right sub-tree
    analyseOutput(root, 5);//delete root with both children
    analyseOutput(root, 2);//delete non-root node with two children in left sub-tree
    analyseOutput(root, 12);//delete non-root leaf node
    analyseOutput(root, 6);//try deleting an element which is not present in the BST
    analyseOutput(root, 15);//try deleting an element which is not present in the BST
    analyseOutput(root, 7);//delete root with both children
    analyseOutput(root, 9);//delete root with no right child
    analyseOutput(root, 4);//delete root with no right child
    analyseOutput(root, 1);//delete the root node
    analyseOutput(root, 4);//try deleting a node from empty tree

    cout<<"\n\nRecreated the tree!\n";
    root = createBST();
    analyseOutput(root, 1);//delete non-root leaf node
    analyseOutput(root, 2);//delete non-root node with no left child in left sub-tree
    analyseOutput(root, 4);//delete the only node in left sub-tree
    analyseOutput(root, 5);//delete root with no left child
    analyseOutput(root, 15);//delete non-root node with no left child in right sub-tree
    analyseOutput(root, 7);//delete non-root leaf node
    analyseOutput(root, 7);//try deleting an element which is not present in the BST
    analyseOutput(root, 6);//delete root with no left child
    analyseOutput(root, 9);//delete root with no left child
    analyseOutput(root, 12);//delete the root node
    return 0;
}



